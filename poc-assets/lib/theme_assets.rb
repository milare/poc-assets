class ThemeAssets
  def initialize(theme)
    @theme = theme
  end

  def stylesheet_file
    File.join 'themes', "#{@theme.name.downcase}.css"
  end

  def javascript_file
    File.join 'themes', "#{@theme.name.downcase}.js"
  end

  def image_file(image)
    File.join 'themes', "#{@theme.name.downcase}_#{image.filename}"
  end

  def sass_file_path
    Rails.root.join('app', 'assets', 'stylesheets', "#{self.stylesheet_file}.scss")
  end

  def js_file_path
    Rails.root.join('app', 'assets', 'javascripts', "#{self.javascript_file}")
  end

  def image_file_path(image)
    Rails.root.join('app', 'assets', 'images', "#{self.image_file(image)}")
  end

  def compile_all
    parsed_css = @theme.css
    @theme.images.each do |image|
      img = compile(image.data, image_file_path(image), image_file(image))
      if img.present?
        parsed_css = parsed_css.gsub(image.filename, img.gsub('themes/', ''))
      else
        parsed_css = parsed_css.gsub(image.filename, "#{@theme.name.downcase}_#{image.filename}")
      end
    end
    compile(@theme.js, js_file_path, javascript_file)
    compile(parsed_css, sass_file_path, stylesheet_file)
  end

  private
  def compile(data, non_compiled_file, compiled_file)
    f = File.new(non_compiled_file, 'w')
    f.write(data)
    f.close

    unless Rails.application.config.assets.compile
      env = Rails.application.assets.is_a?(Sprockets::Index) ? Rails.application.assets.instance_variable_get('@environment') : Rails.application.assets

      Sprockets::StaticCompiler.new(
        env,
        File.join(Rails.public_path, Rails.application.config.assets.prefix),
        [compiled_file],
        digest:   true,
        manifest: false
      ).compile

      Rails.application.config.assets.digests[compiled_file] = env[compiled_file].digest_path
    end
  end
end
