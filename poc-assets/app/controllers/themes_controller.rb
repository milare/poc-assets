class ThemesController < ApplicationController

  def index
    @themes = Theme.all
  end

  def new
    @theme = Theme.new
    @theme.images.build
  end

  def show
    @theme = Theme.find(params[:id])
    @assets = ThemeAssets.new(@theme)
  end

  def create
    @theme = Theme.new(params[:theme])
    @theme.save
    update_images
    redirect_to themes_path
  end

  def edit
    @theme = Theme.find(params[:id])
    @theme.images.build
  end

  def update
    @theme = Theme.find(params[:id])
    @theme.update_attributes(params[:theme])
    update_images
   redirect_to themes_path
  end

  def destroy
    @theme = Theme.find(params[:id])
    @theme.destroy if @theme
    redirect_to themes_path
  end

  private
  def update_images
    (params[:images] || []).each do |image|
      @theme.images << Image.new(filename: image.original_filename, data: File.read(image.tempfile))
    end
    @theme.save
  end
end
