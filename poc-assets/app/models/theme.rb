class Theme
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :css , type: String
  field :js  , type: String

  embeds_many :images

  accepts_nested_attributes_for :images
  after_save :compile_assets

  def compile_assets
    assets = ThemeAssets.new(self)
    assets.compile_all
  end
end
