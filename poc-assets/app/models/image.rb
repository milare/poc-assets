class Image
  include Mongoid::Document

  field :filename, type: String
  field :data, type: String

  embedded_in :theme, :inverse_of => :images
end


