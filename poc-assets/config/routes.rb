PocAssets::Application.routes.draw do

  resources :themes

  root :to => 'themes#index'

end
